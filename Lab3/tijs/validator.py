from pyshacl import validate

# Validate the data graph against the SHACL graph
with open('nobel2.ttl', 'r') as f:
    data_graph = f.read()

with open('nobelshacl.ttl', 'r') as f:
    shacl_graph = f.read()

r = validate(data_graph,
      shacl_graph=shacl_graph,
      inference='rdfs',
      abort_on_first=False,
      allow_infos=False,
      allow_warnings=False,
      meta_shacl=False,
      advanced=False,
      js=False,
      debug=False)

conforms, results_graph, results_text = r
if conforms:
    print('The data conforms to the SHACL shapes')
else:
    print(f'The data does not conform to the SHACL shapes: {results_text}')
