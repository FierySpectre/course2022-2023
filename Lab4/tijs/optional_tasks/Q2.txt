Get all the properties, except for rdf:type, applicable to instances of the class Politician

prefix db:<http://dbpedia.org/ontology/>
select distinct ?p where{
    ?v a db:Politician.
    ?v ?p ?x.
    
}