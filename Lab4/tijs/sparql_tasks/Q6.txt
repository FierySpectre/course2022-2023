Get different places with their number of inhabitants, ordered by the name of the place

prefix gp: <http://GP-onto.fi.upm.es/exercise2#>
select ?c ?s where {
    ?c gp:hasInhabitantNumber ?s
}
ORDER BY ?c